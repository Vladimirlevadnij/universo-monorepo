import { EndlessCanvas } from '../spaces-2d/EndlessCanvas';

const CreateTextObject = (
  scene: EndlessCanvas,
  text: string,
  fontSize: string = '20px',
): any =>
  scene.add.text(0, 0, text, {
    fontSize,
  });
export default CreateTextObject;
