//@ts-nocheck
import GetMaxTextObjectSize from 'src/apps/kanbans/GetMaxTextObjectSize';
import { EndlessCanvas } from '../spaces-2d/EndlessCanvas';
import { LocalStorage } from 'quasar';
import { FONT_EXTRA_SMALL } from 'src/apps/kanbans/Const';

export default function (scene: EndlessCanvas) {
  if (LocalStorage.has('debug')) {
    if (scene.debug?.length) {
      scene.debug.forEach((item) => {
        item.destroy();
      });
      scene.debug.length = 0;
    } else {
      scene.debug = [];
    }
    const sign = '+';
    const pointer0 = scene.input.manager.pointers[0];
    const keys = scene.keys;
    const keysArr = Object.keys(keys).map(
      (key) => `${key}:${keys[key].isDown}`,
    );

    const stringArray = [
      `${pointer0.worldX.toFixed(0)}x${pointer0.worldY.toFixed(0)}`,
      `${pointer0.x}x${pointer0.y}`,
      `button:${pointer0.button}`,
      `isInputMode:${scene.isInputMode}`,
      ...keysArr,
    ];
    const maxSize = GetMaxTextObjectSize(scene, stringArray, FONT_EXTRA_SMALL);

    scene.debug.push(
      scene.add.text(
        scene.cameras.getCamera('ui').width - maxSize.width,
        scene.cameras.getCamera('ui').height -
          maxSize.height * stringArray.length,
        stringArray.join('\n'),
        { fontSize: FONT_EXTRA_SMALL },
      ),
    );

    scene.debug.push(scene.add.text(5, 5, sign));
    scene.debug.push(scene.add.text(scene.cameras.main.width - 5, 5, sign));
    scene.debug.push(
      scene.add.text(
        scene.cameras.main.width - 5,
        scene.cameras.main.height - 5,
        sign,
      ),
    );
    scene.debug.push(scene.add.text(5, scene.cameras.main.height - 5, sign));
    scene.debug.push(
      scene.add.text(
        scene.cameras.main.width / 2,
        scene.cameras.main.height / 2,
        sign,
      ),
    );
    scene.cameras.main.ignore(scene.debug);
    return;
  }
  if (scene.debug?.length === 0) {
    return;
  }
  if (!Array.isArray(scene.debug)) {
    return;
  }
  scene.debug.forEach((item) => {
    item.destroy();
  });
  scene.debug.length = 0;
  delete scene.debug;
}
